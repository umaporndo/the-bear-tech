@extends('layouts.app')

@section('page-title', __('tech.page_title.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name ,
        ]))
@section('page-description', __('tech.page_description.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name ,
        ]))
@section('page-keyword', __('tech.page_keyword.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name ,
        ]))

@section('og-image', asset(config('images.open_graph.default_image')))
@section('og-title', __('tech.og_title.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name,
        ]))
@section('og-description', __('tech.og_description.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name,
        ]))
@section('og-keyword', __('tech.og_keyword.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name,
        ]))
@section('og-url', __('tech.og_url.menu',[
            'menu_id' => $contentList[0]['Menu']->id,
            'slug' => str_replace(' ', '-', $contentList[0]['Menu']->menu_name),
        ]) )

@section('content')
    <div class="content-desktop">
    </div>
    <div>
        <div class="container content-detail" style="margin-top:70px;">
            @if($contentList->total() !==  0)
                <h1>{{ $contentList[0]['Menu']->menu_name }}</h1>
                <div id="content-list-box">
                    @include('tech.menu_list')

                </div>
            @else
                <h3>Empty Content</h3>
            @endif

        </div>
    </div>
@endsection
