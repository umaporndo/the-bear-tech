<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Travel Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index'  => 'The bear tech',
        'detail' => 'The bear tech',
    ],

    'page_title' => [
        'index'  => 'The bear tech',
        'detail' => ':tech_title | The bear tech',
        'menu'   => 'The bear tech | :menu_title',
    ],

    'page_description' => [
        'index'  => 'The bear tech',
        'detail' => 'The bear tech :tech_description',
        'menu'   => 'The bear tech | :menu_title',
    ],

    'page_keyword' => [
        'index'  => 'The bear tech',
        'detail' => 'The bear tech :tech_keyword',
        'menu'   => 'The bear tech | :menu_title',
    ],

    'og_title' => [
        'index'  => 'The bear tech',
        'detail' => ':tech_title | The bear tech',
        'menu'   => 'The bear tech | :menu_title',
    ],

    'og_description' => [
        'index'  => 'The bear tech',
        'detail' => ':tech_description',
        'menu'   => 'The bear tech | :menu_title',
    ],

    'og_keyword' => [
        'index'  => 'The bear tech',
        'detail' => ':tech_keyword',
        'menu'   => 'The bear tech | :menu_title',
    ],

    'og_url' => [
        'index'  => route( 'tech.index' ),
        'detail' => route( 'tech.detail', [ 'slug' => ':slug', 'id' => ':tech_id' ] ),
        'menu'   => route( 'tech.menu', [ 'slug' => ':slug', 'menuID' => ':menu_id' ] ),
    ],

];
